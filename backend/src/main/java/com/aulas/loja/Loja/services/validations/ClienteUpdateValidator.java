package com.aulas.loja.Loja.services.validations;

import com.aulas.loja.Loja.domain.Cliente;
import com.aulas.loja.Loja.domain.enums.TipoCliente;
import com.aulas.loja.Loja.dto.ClienteDTO;
import com.aulas.loja.Loja.repositories.ClienteRepository;
import com.aulas.loja.Loja.resources.exception.FieldMessage;
import com.aulas.loja.Loja.services.validations.utils.BR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClienteUpdateValidator implements ConstraintValidator<ClienteUpdate, ClienteDTO> {

    @Autowired
    private HttpServletRequest request;  //permite recuperar o parametro id do Cliente que está na uri

    @Autowired
    private ClienteRepository cliRepo;

    @Override
    public void initialize(ClienteUpdate ann) {
    }

    @Override // MÉTODO PARA IMPLEMENTAR A VALIDACAO E CPF OU CNPJ
    public boolean isValid(ClienteDTO objDto, ConstraintValidatorContext context) {

        //mapeia os atributos da uri que é recebida pelo request
        @SuppressWarnings("unchecked")
        Map<String, String> map = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        Integer uriId = Integer.parseInt(map.get("id"));

        List<FieldMessage> list = new ArrayList<>();

        Cliente aux = cliRepo.findByEmail(objDto.getEmail());
        if(aux != null && !aux.getId().equals(uriId)) {
            list.add(new FieldMessage("email", "Email já existente"));
        }

        for (FieldMessage e: list) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName()).addConstraintViolation();
        }
        return list.isEmpty();
    }

}
