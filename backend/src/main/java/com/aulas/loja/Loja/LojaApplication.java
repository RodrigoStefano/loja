package com.aulas.loja.Loja;

import com.aulas.loja.Loja.domain.*;
import com.aulas.loja.Loja.domain.enums.EstadoPagamento;
import com.aulas.loja.Loja.domain.enums.TipoCliente;
import com.aulas.loja.Loja.repositories.*;
import com.aulas.loja.Loja.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@SpringBootApplication
public class LojaApplication implements CommandLineRunner {

	@Autowired
	private CategoriaRepository CatRepo;

	@Autowired
	private ProdutoRepository ProdRepo;

	@Autowired
	private EstadoRepository EstRepo;

	@Autowired
	private CidadeRepository CidRepo;

	@Autowired
	private ClienteRepository CliRepo;

	@Autowired
	private EnderecoRepository EndRepo;

	@Autowired
	private PedidoRepository PedRepo;

	@Autowired
	private PagamentoRepository PagRepo;

	@Autowired
	private ItemPedidoRepository ItemPedidoRepo;

	public static void main(String[] args) {
		SpringApplication.run(LojaApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		Categoria cat1 = new Categoria(null, "Informática");
		Categoria cat2 = new Categoria(null, "Escritório");
		Categoria cat3 = new Categoria(null, "Cama mesa e banho");
		Categoria cat4 = new Categoria(null, "Eletrônicos");
		Categoria cat5 = new Categoria(null, "Jardinagem");
		Categoria cat6 = new Categoria(null, "Decoração");
		Categoria cat7 = new Categoria(null, "Perfumaria");
		Categoria cat8 = new Categoria(null, "Ferramentas");
		Categoria cat9 = new Categoria(null, "Tintas");
		Categoria cat10 = new Categoria(null, "Construção");



		Produto p1 = new Produto(null, "Computador", 2000.00);
		Produto p2 = new Produto(null, "Impressora", 800.00);
		Produto p3 = new Produto(null, "Mouse", 80.00);
		Produto p4 = new Produto(null, "Mesa de Escritório", 300.00);
		Produto p5 = new Produto(null, "Toalha", 50.00);
		Produto p6 = new Produto(null, "Colcha", 200.00);
		Produto p7 = new Produto(null, "Tv true color", 1200.00);
		Produto p8 = new Produto(null, "Roçadeira", 800.00);
		Produto p9 = new Produto(null, "Abajour", 100.00);
		Produto p10 = new Produto(null, "One Million", 180.00);
		Produto p11 = new Produto(null, "Shampoo", 90.00);

		cat1.getProdutos().addAll(Arrays.asList(p1,p2,p3));
		cat2.getProdutos().addAll(Arrays.asList(p2,p4));
		cat3.getProdutos().addAll(Arrays.asList(p5,p6));
		cat4.getProdutos().addAll(Arrays.asList(p1,p2,p3,p7));
		cat5.getProdutos().add((p8));
		cat6.getProdutos().add(p9);
		cat7.getProdutos().add(p10);
		cat8.getProdutos().add(p8);


		p1.getCategorias().addAll(Arrays.asList(cat1, cat4));
		p2.getCategorias().addAll(Arrays.asList(cat1, cat2, cat4));
		p3.getCategorias().addAll(Arrays.asList(cat1, cat4));
		p4.getCategorias().add(cat2);
		p5.getCategorias().add(cat3);
		p6.getCategorias().add(cat3);
		p7.getCategorias().add(cat4);
		p8.getCategorias().addAll(Arrays.asList(cat5, cat8));
		p9.getCategorias().add(cat6);
		p10.getCategorias().add(cat7);
		p11.getCategorias().add(cat3);


		CatRepo.saveAll(Arrays.asList(cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8, cat9, cat10));
		ProdRepo.saveAll(Arrays.asList(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11));

		Estado e1 = new Estado(null, "Minas Gerais");
		Estado e2 = new Estado(null, "São Paulo");
		Estado e3 = new Estado(null, "Rondônia");
		Estado e4 = new Estado(null, "Paraíba");

		Cidade c1 = new Cidade (null, "Uberlandia", e1);
		Cidade c2 = new Cidade (null, "Campinas", e2);
		Cidade c3 = new Cidade (null, "São Paulo", e2);
		Cidade c4 = new Cidade (null, "Porto Velho", e3);
		Cidade c5 = new Cidade (null, "João Pessoa", e4);

		e1.getCidades().addAll(Arrays.asList(c1));
		e2.getCidades().addAll(Arrays.asList(c2,c3));
		e3.getCidades().addAll(Arrays.asList(c4));
		e4.getCidades().addAll(Arrays.asList(c5));

		EstRepo.saveAll(Arrays.asList(e1,e2,e3,e4));
		CidRepo.saveAll(Arrays.asList(c1,c2,c3,c4,c5));

		Cliente cli1 = new Cliente(null, "Maria Silva", "maria@gmail.com", "00989745654", TipoCliente.PESSOAFISICA);
		cli1.getTelefones().addAll(Arrays.asList("99990129", "92921282"));

		Endereco end1 = new Endereco(null, "Rua Projetada", "150","Ao lado da farmácia", "Jardim Primavera", "76812199", cli1, c1);
		Endereco end2 = new Endereco(null, "Rua 11", "230","próximo ao banco", "Centro", "76812100", cli1, c2);

		cli1.getEnderecos().addAll(Arrays.asList(end1, end2));

		CliRepo.saveAll(Arrays.asList(cli1));
		EndRepo.saveAll(Arrays.asList(end1,end2));

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Pedido ped1 = new Pedido(null,sdf.parse("29/03/2019 11:00"), cli1, end1);
		Pedido ped2 = new Pedido(null,sdf.parse("30/03/2019 12:00"), cli1, end2);

		Pagamento pag1 = new PagamentoComCartao(null, EstadoPagamento.QUITADO, ped1, 6);
		ped1.setPagamento(pag1);

		Pagamento pag2 = new PagamentoComBoleto(null, EstadoPagamento.PENDENTE, ped2, sdf.parse("30/03/2019"), null);
		ped2.setPagamento(pag2);

		cli1.getPedidos().addAll(Arrays.asList(ped1,ped2));

		PedRepo.saveAll(Arrays.asList(ped1,ped2));
		PagRepo.saveAll(Arrays.asList(pag1, pag2));

		ItemPedido ip1 = new ItemPedido(ped1, p1, 0.00, 1,2000.00);
		ItemPedido ip2 = new ItemPedido(ped1, p3, 0.00, 2,80.00);
		ItemPedido ip3 = new ItemPedido(ped2, p2, 100.00, 1,800.00);

		ped1.getItens().addAll(Arrays.asList(ip1,ip2));
		ped2.getItens().addAll(Arrays.asList(ip3));

		p1.getItens().addAll(Arrays.asList(ip1));
		p2.getItens().addAll(Arrays.asList(ip3));
		p3.getItens().addAll(Arrays.asList(ip2));

		ItemPedidoRepo.saveAll(Arrays.asList(ip1,ip2,ip3));

	}
}
