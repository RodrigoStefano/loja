package com.aulas.loja.Loja.services;

import com.aulas.loja.Loja.domain.Pedido;
import com.aulas.loja.Loja.services.exceptions.ObjectNotFoundException;
import com.aulas.loja.Loja.repositories.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PedidoService {

    @Autowired
    private PedidoRepository repo;

    public Pedido find(Integer id){
        Optional<Pedido> obj = repo.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException(
                "Pedido não encontrado! Id: " + id + ", Tipo: " + Pedido.class.getName()));
    }

    public List<Pedido> findAll() {
        return repo.findAll();
    }

}
