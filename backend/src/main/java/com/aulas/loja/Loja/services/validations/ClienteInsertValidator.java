package com.aulas.loja.Loja.services.validations;

import com.aulas.loja.Loja.domain.Cliente;
import com.aulas.loja.Loja.domain.enums.TipoCliente;
import com.aulas.loja.Loja.dto.ClienteNewDTO;
import com.aulas.loja.Loja.repositories.ClienteRepository;
import com.aulas.loja.Loja.resources.exception.FieldMessage;
import com.aulas.loja.Loja.services.validations.utils.BR;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class ClienteInsertValidator implements ConstraintValidator<ClienteInsert, ClienteNewDTO> {

    @Autowired
    private ClienteRepository cliRepo;

    @Override
    public void initialize(ClienteInsert ann) {
    }

    @Override // MÉTODO PARA IMPLEMENTAR A VALIDACAO E CPF OU CNPJ
    public boolean isValid(ClienteNewDTO objDto, ConstraintValidatorContext context) {
        List<FieldMessage> list = new ArrayList<>();

        if(objDto.getTipo().equals(TipoCliente.PESSOAFISICA.getCod()) && !BR.isValidCPF(objDto.getCpfOuCnpj())) {
            list.add(new FieldMessage("cpfOuCnpj", "CPF do cliente inválido"));
        }

        if(objDto.getTipo().equals(TipoCliente.PESSOAJURIDICA.getCod()) && !BR.isValidCNPJ(objDto.getCpfOuCnpj())) {
            list.add(new FieldMessage("cpfOuCnpj", "CNPJ inválido"));
        }

        Cliente aux = cliRepo.findByEmail(objDto.getEmail());
        if(aux != null) {
            list.add(new FieldMessage("email", "Email já existente"));
        }

        for (FieldMessage e: list) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName()).addConstraintViolation();
        }
        return list.isEmpty();
    }

}
