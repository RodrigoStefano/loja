package com.aulas.loja.Loja.repositories;

import com.aulas.loja.Loja.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    @Transactional (readOnly=true) //faz a transação ficar + rápida e diminui o locking no gerenciamento de transações de BD
    Cliente findByEmail(String email);  // findByEmail  o Spring Data detecta automaticamente que quer fazer uma busca e implementa o método
}
